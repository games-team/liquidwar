Source: liquidwar
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Barry deFreese <bdefreese@debian.org>,
 Eduard Bloch <blade@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 liballegro4-dev,
 python3
Standards-Version: 4.5.0
Homepage: https://ufoot.org/liquidwar/v5
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/games-team/liquidwar.git
Vcs-Browser: https://salsa.debian.org/games-team/liquidwar

Package: liquidwar
Architecture: any
Depends:
 liquidwar-data (= ${source:Version}),
 liquidwar-server (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: truly original multiplayer wargame
 Liquid War is an original multiplayer wargame. There are no weapons, the only
 thing you have to do is to move a cursor in a 2-D battlefield. This cursor is
 followed by your army, which is composed by a  great many little fighters,
 represented by small colored squares. When fighters from different teams meet,
 they eat each other, it is as simple as that.
 .
 A single player mode is available, but the game is definitely designed to be
 multiplayer, and has network support.

Package: liquidwar-data
Architecture: all
Depends:
 ${misc:Depends}
Description: data files for Liquid War
 Liquid War is an original multiplayer wargame. There are no weapons, the only
 thing you have to do is to move a cursor in a 2-D battlefield. This cursor is
 followed by your army, which is composed by a  great many little fighters,
 represented by small colored squares. When fighters from different teams meet,
 they eat each other, it is as simple as that.
 .
 This package holds the data files necessary to play Liquid War, an original
 multiplayer wargame.

Package: liquidwar-server
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Replaces:
 liquidwar (<< 5.6.2-2)
Description: Liquid War server
 Liquid War is an original multiplayer wargame. There are no weapons, the only
 thing you have to do is to move a cursor in a 2-D battlefield. This cursor is
 followed by your army, which is composed by a  great many little fighters,
 represented by small colored squares. When fighters from different teams meet,
 they eat each other, it is as simple as that.
 .
 Liquid War is an original multiplayer wargame. This package contains the
 server for Liquid War game.
